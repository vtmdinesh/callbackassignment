const fs = require("fs")
const path = require('path')



const problem2 = (textFilePath) => {




    const deleteFiles = (fileName) => {
        let filePath = path.join(__dirname, fileName)
        fs.readFile(filePath, { encoding: "utf-8", flag: "r" }, (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                let fileNameArray = data.split("\n")
                fileNameArray.map(fileName => {

                    let filePathToDel = path.join(__dirname, fileName)
                    fs.unlink(filePathToDel, (err) => {
                        if (err) {
                            console.error(err)
                        }
                        else {
                            console.info(`${fileName} deleted SuccessFully`)
                        }
                    })

                })

            }
        })
    }


    const getSortedData = (data) => {

        let splittedData = data.split("\n");
        let sortedData = splittedData.sort();
        sortedData = sortedData.join("\n");
        return sortedData

    }


    const createJoinedFile = (upperCaseFileName, splittedFileName) => {

        let upperCaseFilePath = path.join(__dirname, upperCaseFileName)
        let splittedFilePath = path.join(__dirname, splittedFileName)
        let joinedFileName = "joinedFile.txt"
        let joinedFilePath = path.join(__dirname, joinedFileName)
        let sortedData = null;
        fs.readFile(upperCaseFilePath, { encoding: "utf-8", flag: "r" }, (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                sortedData = getSortedData(data)
                fs.writeFile(joinedFilePath, sortedData, (err) => {
                    if (err) {
                        console.error(err)
                    }
                })
                fs.readFile(splittedFilePath, { encoding: "utf-8", flag: "r" }, (err, data) => {
                    if (err) {
                        console.error(err)
                    }
                    else {
                        sortedData = getSortedData(data)
                        fs.appendFile(joinedFilePath, sortedData, (err) => {
                            if (err) {
                                console.error(err)
                            }
                            else {
                                let filePath = path.join(__dirname, "fileNames.txt")
                                fs.appendFile(filePath, "\njoinedFile.txt", (err) => {
                                    if (err) {
                                        console.log(error)
                                    }
                                    else {
                                        console.info("joinedFile.txt added successfully")
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }

    const createSplittedLowerCaseFile = () => {

        let upperCaseFileName = "upperCaseFile.txt"
        let upperCasefilePath = path.join(__dirname, upperCaseFileName)



        fs.readFile(upperCasefilePath, "utf-8", (err, data) => {
            if (err) {
                console.error(err)
            }
            else {
                let splittedFileName = "splittedFile.txt"
                let splittedFilePath = path.join(__dirname, splittedFileName)
                let convertedData = data.toLowerCase();
                let splittedData = convertedData.split(".");
                splittedData = splittedData.map((eachSentence) =>
                    eachSentence.trim()
                );

                convertedData = splittedData.join(".\n");

                fs.writeFile(splittedFilePath, convertedData, (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        let filePath = path.join(__dirname, "fileNames.txt")
                        fs.appendFile(filePath, "\n" + splittedFileName, (err) => {
                            if (err) {
                                console.log(error)
                            }
                            else {
                                console.log(splittedFileName, "added successfully")
                            }
                        })
                    }
                })

            }
        })




    }

    const createUpperCaseFile = (data) => {
        let upperCaseFileName = "upperCaseFile.txt"
        let upperCaseData = data.toUpperCase()
        upperCaseData = upperCaseData.split("/n/n").join(" /n");
        let filePath = path.join(__dirname, upperCaseFileName)
        fs.writeFile(filePath, upperCaseData, (err) => {
            if (err) {
                console.error(err)
            }
            else {
                fs.writeFile("../fileNames.txt", upperCaseFileName, (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.info(upperCaseFileName, "added successfully")
                    }
                })
            }
        })
    }

    fs.readFile(textFilePath, { encoding: "utf-8", flag: 'r' }, (err, data) => {
        if (err) {
            console.error(err)
        }
        else {
            let filePath = path.join(__dirname, "../", "fileNames.txt")
            fs.writeFile(filePath, "", (err) => {
                if (err) {
                    console.log(err)
                }
                else {
                    createUpperCaseFile(data)

                    setTimeout(() => {
                        createSplittedLowerCaseFile()
                    }, 2000)

                }
            })
        }
    })
    setTimeout(() => {
        createJoinedFile("upperCaseFile.txt", "splittedFile.txt")
    }, 3000)


    setTimeout(() => {
        deleteFiles("fileNames.txt")
    }, 6000)
}


module.exports = problem2