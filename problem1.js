const problem1 = (folderName, numberOfFilesToBeCreated,) => {
    const fs = require("fs");
    const path = require("path")

    const fileWrite = (dirPath) => {
        let data = "This is a file containing a collection of books.";
        let n = 1
        while (n <= numberOfFilesToBeCreated) {
            fileName = "sampleFile" + n + ".json"
            filePath = path.join(dirPath, fileName)

            fs.writeFile(filePath, data, (err) => {
                if (err) {
                    console.log(err)
                }
                else {
                    console.log('File creation success and data added');
                }
            });
            n++
        }
    }

    const fileDelete = (dirPath) => {
        fs.readdir(dirPath, (err, files) => {
            if (err)
                console.log(err);
            else {
                files.map((file) => {

                    filePath = path.join(dirPath, file)
                    fs.unlink(filePath, (err) => {
                        if (!err) {
                            console.log("File Deleted")
                        }
                        else {
                            console.log(err)
                        }
                    })
                })
            }
        })
    }

    const createDir = (fileWrite, dirName) => {
        let dirPath = path.join(__dirname, dirName)
        fs.mkdirSync(dirPath, { recursive: true }, function (err) {
            if (err) {
                console.log(err)
            }
            else {
                console.log('Folder creation success and data added');
            }
        })
        setTimeout(() => {
            fileWrite(dirPath)
        }, 1000)

        setTimeout(() => {
            fileDelete(dirPath)
        }, 3000)
    }

    const main = (folderName => {
        createDir(fileWrite, folderName)
    })

    main(folderName);

}

module.exports = problem1